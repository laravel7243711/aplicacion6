<?php

namespace Database\Seeders;

use App\Models\ProductosTiendas;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductosTiendasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        ProductosTiendas::factory(20)->create();
    }
}
