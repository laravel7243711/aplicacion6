<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Tiendas;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Tiendas>
 */
class TiendasFactory extends Factory
{
    protected $model=Tiendas::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
         'id'=>$this->faker->id(),
         'nombre'=>$this->faker->nombre(),
         'ubicacion'=>$this->faker->ubicacion(),
        
        ];
    }
}
