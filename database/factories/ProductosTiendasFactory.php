<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\ProductosTiendas;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ProductosTiendas>
 */
class ProductosTiendasFactory extends Factory
{
    protected $model=ProductosTiendas::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id'=>$this->faker->id(),
            'id_producto'=>$this->faker->id(),
            'id_tienda'=>$this->faker->id(),
            'cantidad'=>$this->faker->cantidad(),

          
        ];
    }
}
