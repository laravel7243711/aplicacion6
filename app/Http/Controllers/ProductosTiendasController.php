<?php

namespace App\Http\Controllers;

use App\Models\ProductosTiendas;
use Illuminate\Http\Request;

class ProductosTiendasController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ProductosTiendas $productosTiendas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ProductosTiendas $productosTiendas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ProductosTiendas $productosTiendas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ProductosTiendas $productosTiendas)
    {
        //
    }
}
